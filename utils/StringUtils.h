#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include <sstream>


#ifndef __MYSTRING__
#define __MYSTRING__

std::vector<std::string> getLimits(std::string &str, char separator);

#endif /* __MYSTRING__ */
