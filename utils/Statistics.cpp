#include "Statistics.h"

int
compare(const void * a, const void * b)
{
  float x1 = *(float *) a;
  float x2 = *(float *) b;
  if (x1 == x2)
    return 0;
  if (x1 < x2)
    return -1;
  else
    return 1;
}

Statistics::Statistics(int numTests)
{
    this->numTests = numTests;
    this->counter = 0;
    this->times = (float *) malloc(this->numTests * sizeof(float));
    this->toDiscard = 0.0f;
}

Statistics::Statistics(int numTests, float toDiscard)
{
    this->numTests = numTests;
    this->counter = 0;
    this->times = (float *) malloc(this->numTests * sizeof(float));
    this->toDiscard = toDiscard;
}

void Statistics::addTime(float time)
{
    if(this->counter < this->numTests)
        this->times[this->counter++] = time;
    else
        throw counter;
    if(this->counter == this->numTests)
        qsort(this->times,this->numTests,sizeof(float),compare);
}

float Statistics::getAverage()
{
    if(this->counter != this->numTests)
        throw this->counter;
    float sum = 0.0f;
    int start = this->numTests * this->toDiscard;
    int end = this->numTests - start;
    for(int i = start; i < end; i++) {
        sum += times[i];
    }
    return sum/((float)end - start);
}

float Statistics::getStandardDeviation()
{
    if(this->counter != this->numTests)
        throw this->counter;
    int start = this->numTests * this->toDiscard;
    int end = this->numTests - start;

    float average = this->getAverage();

    float variance = 0.0;
    for (int i = start; i < end; i++)
    {
      variance = variance + pow(this->times[i] - average, 2.0f);
    }
    variance = variance / ((float) (end - start) - 1.0f);
    return pow(variance,0.5f);
}

Statistics::~Statistics()
{
    free(this->times);
}
