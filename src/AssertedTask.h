#ifndef ASSERTEDTASK_H
#define ASSERTEDTASK_H

#include "Task.h"
#include <assert.h>

class AssertedTask: public Task
{
    public:

        /**
         * Default constructor
         */
        AssertedTask():Task(){}

        /**
         * Calls the task.
         */
        virtual void call(ThreadWrapper * thr) {
            this->setWorker(thr);
            this->run();
            assert(this->assertion());
            this->future->end();
        }

        /**
         * Asserts the task executed correctly.
         * @return <code>bool</code> whether the assertion was respected or not.
         */
        virtual bool assertion(void) = 0;

        /**
         * Default destructor
         */
        virtual ~AssertedTask(){};

    protected:
    private:
};

#endif // ASSERTEDTASK_H
